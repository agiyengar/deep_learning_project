import cv2
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy import misc
from skimage import io
import base_data_manager
import utils
from utils import init_logging, log_debug, log_info, log_warning, log_critical, log_exception
import kaggle_data_generator
from sklearn.model_selection import train_test_split
import sklearn
from collections import Counter
from sklearn.utils import class_weight

# CamvidDataManager subclass for the DataManagerBase class. Provides functionality to load and manage the Camvid dataset
class CamvidDataManager(base_data_manager.DataManagerBase):
    def __init__(self, config_file_name):
        self.attributes = {}
        self.config_file_name = config_file_name
        init_logging(self.config_file_name)
        self.attributes["data_path"] =  utils.get_config_value(config_file_name, "Camvid", "dataPath")
        self.attributes["image_width"] =  int(utils.get_config_value(config_file_name, "Camvid", "imageWidth"))
        self.attributes["image_height"] = int(utils.get_config_value(config_file_name, "Camvid", "imageHeight"))
        self.attributes["image_channels"] = int(utils.get_config_value(config_file_name, "Camvid", "imageChannels"))
        self.attributes["mask_threshold"] = int(utils.get_config_value(config_file_name, "Camvid", "maskThreshold"))
        self.attributes["seed"] = int(utils.get_config_value(config_file_name, "Camvid", "imageAugmentationSeed"))

        log_info("Config values read from ini file are")
        log_info("Data path is %s. Image Width %d, Image Height %d" %(self.attributes.get("data_path"),
                                                                self.attributes.get("image_width"), self.attributes.get("image_height")))
        log_info("Done printing config values")
        
        self.train_masks_data_frame = self.init_masks(self.attributes["data_path"], masks_file_name = "train.txt")
        self.train_ids = self.train_masks_data_frame["image_id"].values
        log_info("Train Image ids length is  %d" %(len(self.train_ids)))
        self.train_images = utils.load_images(self.attributes, "train", "", ".png", False, self.train_ids)
        self.train_images_list = list(self.train_images.values())
        self.train_images_list = np.reshape(self.train_images_list, (len(self.train_images_list), self.train_images_list[0].shape[0],
                                                                   self.train_images_list[0].shape[1], self.train_images_list[0].shape[2]))
        log_info("Total training images is %d" %(len(self.train_images)))
        self.train_masks = utils.load_images(self.attributes, "trainannot", "", ".png", True, self.train_ids)
        self.train_masks_list = list(self.train_masks.values())
        self.train_masks_list = np.reshape(self.train_masks_list, (len(self.train_masks_list), self.train_masks_list[0].shape[0],
                                                                   self.train_masks_list[0].shape[1], self.train_masks_list[0].shape[2]))
        log_info("Total training images in list is %d" %(len(self.train_masks_list)))

        #utils.plot_image(self.train_ids[0], self.train_images)
        #utils.plot_image_helper(self.train_images_list[0])
        #utils.plot_image(self.train_ids[0], self.train_masks, 'gray')
        
        self.val_masks_data_frame = self.init_masks(self.attributes["data_path"], masks_file_name = "val.txt")
        self.validate_ids = self.val_masks_data_frame["image_id"].values
        log_info("Val Image ids length is  %d" %(len(self.validate_ids)))
        self.val_images = utils.load_images(self.attributes, "val", "", ".png", False, self.validate_ids)
        self.validate_images_list = list(self.val_images.values())
        self.validate_images_list = np.reshape(self.validate_images_list, (len(self.validate_images_list), 
                                                                   self.validate_images_list[0].shape[0],
                                                                   self.validate_images_list[0].shape[1],
                                                                   self.validate_images_list[0].shape[2]))
        log_info("Total validation images is %d" %(len(self.val_images)))
        self.val_masks = utils.load_images(self.attributes, "valannot", "", ".png", True, self.validate_ids)
        self.validate_masks_list = list(self.val_masks.values())
        self.validate_masks_list = np.reshape(self.validate_masks_list, (len(self.validate_masks_list),
                                                                    self.validate_masks_list[0].shape[0],     
                                                                    self.validate_masks_list[0].shape[1],
                                                                    self.validate_masks_list[0].shape[2]))
        log_info("Total validation mask images in list is %d" %(len(self.validate_masks_list)))

        #utils.plot_image(self.validate_ids[0], self.val_images)
        #utils.plot_image_helper(self.validate_images_list[0])
        #utils.plot_image(self.validate_ids[0], self.val_masks, 'gray')

        self.test_masks_data_frame = self.init_masks(self.attributes["data_path"], masks_file_name = "test.txt")
        self.test_image_ids = self.test_masks_data_frame["image_id"].values
        log_info("Test image ids length is  %d" %(len(self.test_image_ids)))
        self.test_images = utils.load_images(self.attributes, "test", "", ".png", False, self.test_image_ids)
        self.test_images_list = list(self.test_images.values())
        self.test_images_list = np.reshape(self.test_images_list, (len(self.test_images_list), self.test_images_list[0].shape[0],
                                                                   self.test_images_list[0].shape[1], self.test_images_list[0].shape[2]))
        log_info("Total testing images is %d" %(len(self.test_images)))
        self.test_masks = utils.load_images(self.attributes, "testannot", "", ".png", True, self.test_image_ids)
        self.test_masks_list = list(self.test_masks.values())
        self.test_masks_list = np.reshape(self.test_masks_list, (len(self.test_masks_list), self.test_masks_list[0].shape[0],
                                                                   self.test_masks_list[0].shape[1], self.test_masks_list[0].shape[2]))
        log_info("Total testing mask images in list is %d" %(len(self.test_masks_list)))

        #utils.plot_image(self.test_image_ids[0], self.test_images)
        #utils.plot_image_helper(self.test_images_list[0])
        #utils.plot_image(self.test_image_ids[0], self.test_masks, 'gray')

       
        self.datagen_train_images = None
        self.datagen_train_masks = None
        self.datagen_validation_images = None
        self.datagen_validation_masks = None
        self.iter_training_images = None
        self.iter_training_masks = None
        self.iter_validation_images = None
        self.iter_validation_masks = None
        super().__init__(self.attributes["data_path"])

    def get_next_batch_train_image(self, batch_size):
        return self.datagen_train_images.flow(self.train_images_list, self.train_ids, batch_size = batch_size,
                                              seed = self.attributes["seed"])
    
    def get_next_batch_train_masks(self, batch_size):
        return self.datagen_train_masks.flow(self.train_masks_list, self.train_ids, batch_size = batch_size,
                                             seed = self.attributes["seed"])

    def get_next_batch_validate_image(self, batch_size):
        return self.datagen_validation_images.flow(self.validate_images_list, self.validate_ids, batch_size = batch_size, seed = self.attributes["seed"], shuffle = False)
    
    def get_next_batch_validate_masks(self, batch_size):
        return self.datagen_validation_masks.flow(self.validate_masks_list, self.validate_ids, batch_size = batch_size, seed = self.attributes["seed"], shuffle = False)
    
    def get_next_batch_train(self, batch_size):
        log_info("In get_next_batch_train()")

        if (self.iter_training_images == None):
            self.iter_training_images = self.get_next_batch_train_image(batch_size)

        if (self.iter_training_masks == None):
            self.iter_training_masks = self.get_next_batch_train_masks(batch_size)

        while (True):
            train_image_batch = None
            train_masks_batch = None

            count = 0
            for images, image_id in self.iter_training_images:
                train_image_batch = images
                break

            for masks, image_id in self.iter_training_masks:
                train_masks_batch = masks
                break

            train_image_batch = np.array(train_image_batch, np.float32) / 255
            yield(train_image_batch, train_masks_batch)

    def get_next_batch_validate(self, batch_size):
        log_info("In get_next_batch_validate()")

        if (self.iter_validation_images == None):
            self.iter_validation_images = self.get_next_batch_validate_image(batch_size)

        if (self.iter_validation_masks == None):
            self.iter_validation_masks = self.get_next_batch_validate_masks(batch_size)

        while (True):
            validate_image_batch = None
            validate_masks_batch = None

            count = 0
            for images, image_id in self.iter_validation_images:
                validate_image_batch = images
                break

            for masks, image_id in self.iter_validation_masks:
                validate_masks_batch = masks
                break

            validate_image_batch = np.array(validate_image_batch, np.float32) / 255
            yield(validate_image_batch, validate_masks_batch)

    def init_generator(self, batch_type):
        if (batch_type == kaggle_data_generator.BatchType.TRAIN_DATA):
            self.datagen_train_images = kaggle_data_generator.KaggleDataGenerator(batch_type, 'constant').getDatagenerator()
            self.datagen_train_images.fit(self.train_images_list, augment = True, seed = self.attributes["seed"]) 
            self.datagen_train_masks = kaggle_data_generator.KaggleDataGenerator(batch_type, 'constant', 0).getDatagenerator()
            self.datagen_train_masks.fit(self.train_masks_list, augment = True, seed = self.attributes["seed"])
        else:
            self.datagen_validation_images = kaggle_data_generator.KaggleDataGenerator(batch_type, 'constant').getDatagenerator()
            self.datagen_validation_images.fit(self.validate_images_list, augment = True, seed = self.attributes["seed"]) 
            self.datagen_validation_masks = kaggle_data_generator.KaggleDataGenerator(batch_type, 'constant', 0).getDatagenerator()
            self.datagen_validation_masks.fit(self.train_masks_list, augment = True, seed = self.attributes["seed"])
    
    def init_masks(self, data_path, masks_file_name = "train.txt"):
        train_path = data_path + "/" + masks_file_name
        log_info('file pathe %s' %train_path)
        df = pd.read_csv(train_path,header=None, delimiter=' ')
        df.columns = ["path", "annotated_path"]
        df[['col1','col2','col3','col4','col5']] = df.path.str.split("/",expand=True,)
        df[['image_id','col6']] = df.col5.str.split(".png",expand=True,)
        df.drop(['col1','col2','col3','col4','col5','col6'],axis=1, inplace=True)
        #log_info(df["image_id"])
        return df
    
    def get_number_of_training_records(self):
        return len(self.train_images_list)

    def get_number_of_validation_records(self):
        return len(self.validate_images_list)

    def get_class_weights(self):
        dict = {}
        unique = np.unique(self.train_masks_list, return_index = False, return_inverse = False, return_counts = True, axis = None)
        classes = unique[0]
        counts = unique[1]
        for i in range(0, classes.size):
            dict[classes[i]] = counts[i]

        unique = np.unique(self.validate_masks_list, return_index = False, return_inverse = False, return_counts = True, axis = None)
        classes = unique[0]
        counts = unique[1]
        for i in range(0, classes.size):
            dict[classes[i]] += counts[i]

        frequencies = np.array(list(dict.values()))
        med = np.median(frequencies)

        weights = med / frequencies
        return weights
  
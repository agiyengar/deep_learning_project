import numpy as np
from enum import Enum

class BatchType(Enum):
	TRAINING = 1
	VALIDATION = 2

# Base class for the data layer used by the Segnet CNN. It defines an interface which should be implemented
# by its subclasses.
class DataManagerBase:
    def __init__(self, data_path):
        self.data_path = data_path

    def get_data_path(self):
        return self.data_path

    def get_next_batch_train_image(self, batch_size):
        self.not_implemented("get_next_batch_train_image()")

    def get_next_batch_train_masks(self, batch_size):
        self.not_implemented("get_next_batch_train_masks()")

    def get_next_batch_validate_image(self, batch_size):
        self.not_implemented("get_next_batch_validate_image()")

    def get_next_batch_validate_masks(self, batch_size):
        self.not_implemented("get_next_batch_validate_masks()")

    def get_next_batch_train(self, batch_size):
        self.not_implemented("get_next_batch_train()")

    def get_next_batch_validate(self, batch_size):
        self.not_implemented("get_next_batch_train()")

    def init_generator(self,batch_type):
        self.not_implemented("get_generator()")

    def not_implemented(self, method):
        print(method, "not implemented")
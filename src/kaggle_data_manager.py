import cv2
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy import misc
from skimage import io
import base_data_manager
import utils
from utils import init_logging, log_debug, log_info, log_warning, log_critical, log_exception
import kaggle_data_generator
from sklearn.model_selection import train_test_split

# KaggleDataManager subclass for the DataManagerBase class. Provides functionality to load and manage the Kaggle car segmentation
# challenge data.
class KaggleDataManager(base_data_manager.DataManagerBase):
    def __init__(self, config_file_name):
        self.data_path = utils.get_config_value(config_file_name, "General", "dataPath")
        init_logging(config_file_name)

        self.config_file_name = config_file_name
        self.image_width = int(utils.get_config_value(config_file_name, "General", "imageWidth"))
        self.image_height = int(utils.get_config_value(config_file_name, "General", "imageHeight"))
        self.image_channels = int(utils.get_config_value(config_file_name, "General", "imageChannels"))
        self.mask_threshold = int(utils.get_config_value(config_file_name, "General", "maskThreshold"))
        self.seed = int(utils.get_config_value(config_file_name, "General", "imageAugmentationSeed"))

        log_info("Config values read from ini file are")
        log_info("Data path is %s. Image Width %d, Image Height %d" %(self.data_path, self.image_width, self.image_height))
        log_info("Done printing config values")
        
        self.train_masks_data_frame = self.init_masks(self.data_path)

        max_examples = utils.get_config_value(config_file_name, "General", "MaxTrainingExamples")
        if (max_examples != None and max_examples != "-1"):
            self.max_training_examples = int(max_examples)
        else:
            self.max_training_examples = len(self.train_masks_data_frame["image_id"].values)

        log_info("Max training examples to use %d" %self.max_training_examples)

        self.image_ids = self.train_masks_data_frame["image_id"].values[:self.max_training_examples]
        log_info("Image ids length %d" %len(self.image_ids))
        self.train_images = self.load_images(self.data_path, "train", "", ".jpg", False)
        self.train_images_list = list(self.train_images.values())
        self.train_images_list = np.reshape(self.train_images_list, (len(self.train_images_list), self.train_images_list[0].shape[0],
                                                                   self.train_images_list[0].shape[1], self.train_images_list[0].shape[2]))
        log_info("Total training images is %d" %(len(self.train_images)))
        self.train_masks = self.load_images(self.data_path, "train_masks", "_mask", ".gif", True)
        self.train_masks_list = list(self.train_masks.values())
        self.train_masks_list = np.reshape(self.train_masks_list, (len(self.train_masks_list), self.train_masks_list[0].shape[0],
                                                                   self.train_masks_list[0].shape[1], self.train_masks_list[0].shape[2]))
        log_info("Total training images in list is %d" %(len(self.train_masks_list)))

        #utils.plot_image(self.image_ids[0], self.train_images)
        #utils.plot_image_helper(self.train_images_list[0])
        #utils.plot_image(self.image_ids[0], self.train_masks, 'gray')

        self.train_images_list, self.validate_images_list, self.train_ids,  self.validate_ids= train_test_split(self.train_images_list,
                                                                                                                self.image_ids, test_size=0.20,
                                                                                                                random_state=self.seed)
        self.train_masks_list, self.validate_masks_list, self.train_ids,  self.validate_ids= train_test_split(self.train_masks_list,
                                                                                                              self.image_ids, test_size=0.20, 
                                                                                                              random_state=self.seed)
        self.datagen_train_images = None
        self.datagen_train_masks = None
        self.datagen_validation_images = None
        self.datagen_validation_masks = None
        self.iter_training_images = None
        self.iter_training_masks = None
        self.iter_validation_images = None
        self.iter_validation_masks = None
        super().__init__(self.data_path)

    def get_next_batch_train_image(self, batch_size):
        return self.datagen_train_images.flow(self.train_images_list, self.train_ids, batch_size = batch_size, seed = self.seed)
    
    def get_next_batch_train_masks(self, batch_size):
        return self.datagen_train_masks.flow(self.train_masks_list, self.train_ids, batch_size = batch_size, seed = self.seed)

    def get_next_batch_validate_image(self, batch_size):
        return self.datagen_validation_images.flow(self.validate_images_list, self.validate_ids, batch_size = batch_size, seed = self.seed)
    
    def get_next_batch_validate_masks(self, batch_size):
        return self.datagen_validation_masks.flow(self.validate_masks_list, self.validate_ids, batch_size = batch_size, seed = self.seed)

    def get_next_batch_train(self, batch_size):
        log_info("In get_next_batch_train()")

        if (self.iter_training_images == None):
            self.iter_training_images = self.get_next_batch_train_image(batch_size)

        if (self.iter_training_masks == None):
            self.iter_training_masks = self.get_next_batch_train_masks(batch_size)

        while (True):
            train_image_batch = None
            train_masks_batch = None

            count = 0
            for images, image_id in self.iter_training_images:
                train_image_batch = images
                break

            for masks, image_id in self.iter_training_masks:
                train_masks_batch = masks
                break

            train_image_batch = np.array(train_image_batch, np.float32) / 255
            yield(train_image_batch, train_masks_batch)

    def get_next_batch_validate(self, batch_size):
        log_info("In get_next_batch_validate()")

        if (self.iter_validation_images == None):
            self.iter_validation_images = self.get_next_batch_validate_image(batch_size)

        if (self.iter_validation_masks == None):
            self.iter_validation_masks = self.get_next_batch_validate_masks(batch_size)

        while (True):
            validate_image_batch = None
            validate_masks_batch = None

            count = 0
            for images, image_id in self.iter_validation_images:
                validate_image_batch = images
                break

            for masks, image_id in self.iter_validation_masks:
                validate_masks_batch = masks
                break

            validate_image_batch = np.array(validate_image_batch, np.float32) / 255
            yield(validate_image_batch, validate_masks_batch)

    def init_generator(self, batch_type):
        if (batch_type == kaggle_data_generator.BatchType.TRAIN_DATA):
            self.datagen_train_images = kaggle_data_generator.KaggleDataGenerator(batch_type).getDatagenerator()
            self.datagen_train_images.fit(self.train_images_list, augment = True, seed = self.seed) 
            self.datagen_train_masks = kaggle_data_generator.KaggleDataGenerator(batch_type).getDatagenerator()
            self.datagen_train_masks.fit(self.train_masks_list, augment = True, seed = self.seed)
        else:
            self.datagen_validation_images = kaggle_data_generator.KaggleDataGenerator(batch_type).getDatagenerator()
            self.datagen_validation_images.fit(self.validate_images_list, augment = False, seed = self.seed) 
            self.datagen_validation_masks = kaggle_data_generator.KaggleDataGenerator(batch_type).getDatagenerator()
            self.datagen_validation_masks.fit(self.validate_masks_list, augment = False, seed = self.seed)
    
    def init_masks(self, data_path, masks_file_name = "train_masks.csv"):
        train_masks_path = data_path + "/" + masks_file_name
        log_info("Training masks path is %s" %train_masks_path)
        
        train_masks_df = pd.read_csv(train_masks_path)
        
        train_masks_df["image_id"] = train_masks_df["img"].map(lambda x : x[:15])
        return train_masks_df
    
    def load_images(self, data_path, train_folder_name, suffix, ext, masks):
        train_images_path = data_path + "/" + train_folder_name
        log_info("Image path is %s " %(train_images_path))
        log_info("total images are %d " %(len(self.image_ids)))
        
        train_images = {}

        count = 0
        for image_file_name in self.image_ids:
            if (masks == False):
                train_images[image_file_name] = self.load_image_file(train_images_path, image_file_name, suffix, ext)
            else:
                train_images[image_file_name] = self.load_mask_image(train_images_path, image_file_name, suffix, ext)
            count = count + 1
            if (count == self.max_training_examples):
                break
        log_info("Returning training images")
        return train_images
            
    def load_image_file(self, images_path, image_file_name, suffix, ext):
        image_file_path = images_path + "/" + image_file_name + suffix + ext
        #print("Image file path is %s" %(image_file_path))
        img = misc.imread(image_file_path)
        return cv2.resize(img, (self.image_width, self.image_height))
    
    def load_mask_image(self, images_path, image_file_name, suffix, ext):
        image_file_path = images_path + "/" + image_file_name + suffix + ext
        #print("Mask image file path is %s" %(image_file_path))
        mask = io.imread(image_file_path, as_gray = True)
        # Convert the image to a 1/0 mask image. Anything above threshold = 1
        mask[mask > self.mask_threshold] = 1
        mask = cv2.resize(mask, (self.image_width, self.image_height))
        # Add a color channel to the image.
        mask = mask.reshape(mask.shape[0], mask.shape[1], 1)
        return mask

    def get_number_of_training_records(self):
        return len(self.train_images_list)

    def get_number_of_validation_records(self):
        return len(self.validate_images_list)

    def get_validation_images(self):
        return self.validate_images_list

    def get_validation_masks(self):
        return self.validate_masks_list

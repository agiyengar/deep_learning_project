import configparser
import cv2
import matplotlib.pyplot as plt
import numpy as np
import os
import errno
import logging
from logging import Logger
from scipy import misc
from skimage import io

# Logging levels
# CRITICAL	50
# ERROR	40
# WARNING	30
# INFO	20
# DEBUG	10
# NOTSET 0
def get_config_value(config_file, section_name, option_name):
    config = configparser.ConfigParser()
    try:
        with open(config_file) as f:
            config.read_file(f)
            return config.get(section_name, option_name)
    except IOError as e:
        print(e.errno, e.strerror)
    except:
        log_warning("Error occurred while reading key %s from section %s from config file %s" %(option_name, section_name, config_file))
        return None

def plot_training_graphs(fit, title, y_param1, y_param2, y_label1, y_label2):
    plt.plot(fit.history[y_param1], label=y_label1)
    plt.plot(fit.history[y_param2], label=y_label2)
    plt.title(title)
    plt.legend()
    plt.show()

def plot(epochs, title, fit, ylabel, xlabel, y_param1, y_param2, y_label1, y_label2):
    N = np.arange(0, epochs)
    plt.style.use("ggplot")
    plt.figure()
    plt.plot(N, fit.history[y_param1], label=y_label1)
    plt.plot(N, fit.history[y_param2], label=y_label2)
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.legend()
    plt.show()

def plot_image(image_id, map_images, cmap = None):
    log_info("Here in plot_image()")
    log_info(image_id)
    image = np.squeeze(map_images[image_id])
    image = image.astype(np.uint8)
    plot = plt.imshow(image, cmap)
    #plt.axis('off')
    plt.show()

def plot_image_helper(image, cmap=None):
    log_info("Here in plot_image_helper()")
    image = np.squeeze(image)
    image = image.astype(np.uint8)
    plot = plt.imshow(image, cmap = cmap)
    #plt.axis('off')
    plt.show()

def plot_image_with_mask(image, mask):
    log_info("Here in plot_image_with_mask()")
    mask_rgb = cv2.cvtColor(mask, cv2.COLOR_GRAY2RGB)
    log_info("Mask shape is ", mask_rgb.shape)
    mask_rgb[mask_rgb == 255] = 1 
    masked_image = image * mask_rgb
    plot_image_helper(masked_image)

def plot_images(images, cmaps, block = True):
   log_info("In plot_images. Total images %d" %len(images))

   plot = plt.figure()
   for i in range(len(images)):
       plot.add_subplot(1, len(images), i + 1)
       image = np.squeeze(images[i])
       if (cmaps[i] == 'gray'):
           image = image.astype(np.uint8)
       plt.imshow(image, cmap = cmaps[i])
   plt.show(block = block)

# Taken from https://stackoverflow.com/a/600612/119527
def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc: # Python >2.5
        if exc.errno == errno.EEXIST and os.path.isdir(path):
            pass
        else: raise

def init_logging(config_file_name):
    logging_level = int(get_config_value(config_file_name, "General", "DebugLogLevel"))
    print("Logging level is ", logging_level)
    logging.basicConfig(level=logging_level)

def log_debug(msg, *args, **kwargs):
    logging.debug(msg, *args, **kwargs)

def log_info(msg, *args, **kwargs):
    logging.info(msg, *args, **kwargs)

def log_warning(msg, *args, **kwargs):
    logging.warning(msg, *args, **kwargs)

def log_critical(msg, *args, **kwargs):
    logging.critical(msg, *args, **kwargs)

def log_exception(msg, *args, **kwargs):
    logging.exception(msg, *args, **kwargs)

def load_images(attributes, folder_name, suffix, ext, masks, image_ids):
    images_path = attributes["data_path"] + "/" + folder_name
    log_info("Image path is %s " %(images_path))
    log_info("total images are %d " %(len(image_ids)))
    
    images = {}

    for image_file_name in image_ids:
        if (masks == False):
            images[image_file_name] = load_image_file(images_path, image_file_name, suffix, ext, attributes)
        else:
            images[image_file_name] = load_mask_image(images_path, image_file_name, suffix, ext,attributes)
    log_info("Returning training images")
    return images
        
def load_image_file(images_path, image_file_name, suffix, ext,attributes):
    image_file_path = images_path + "/" + image_file_name + suffix + ext
    #log_info("Image file path is %s" %(image_file_path))
    img = misc.imread(image_file_path)
    return cv2.resize(img, (attributes["image_width"], attributes["image_height"]))

def load_mask_image(images_path, image_file_name, suffix, ext, attributes):
    image_file_path = images_path + "/" + image_file_name + suffix + ext
    #log_info("Mask image file path is %s" %(image_file_path))
    mask = io.imread(image_file_path, as_gray = True)
    # Convert the image to a 1/0 mask image. Anything above threshold = 1
    mask[mask > attributes["mask_threshold"]] = 1
    mask = cv2.resize(mask, (attributes["image_width"], attributes["image_height"]))
    # Add a color channel to the image.
    mask = mask.reshape(mask.shape[0], mask.shape[1], 1)
    return mask

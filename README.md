Semantic Segmentation using SegNet
============================================

Segment images in the Kaggle Carvana car image data set and the Camvid road scene datasets using a deep learning model based on the 
SegNet architecture.

## Description
This project is based on the paper from Vijay Badrinarayanan, Alex Kendall, Roberto Cipolla titled SegNet: A Deep Convolutional Encoder-Decoder Architecture for Image Segmentation.
We built a fully convolutional neural network whose architecture is similar to the SegNet network architecture described in the above paper.

The code was written and tested in Python version 3.6.5(Anaconda) and was mostly tested on Windows 10 and AWS Ubuntu 4 GPU 64 CPU instance. Please note that the Carvana dataset
is pretty large and takes a long time to train on a non GPU machine. The code to train and test the models was written in Jupyter notebooks.

We used Keras, OpenCV, tensorflow, scipy, etc in our code. Please refer to the imports.txt file in the src folder for details on what packages are needed to run the notebooks.

## Code layout

1. The src folder contains the python notebooks used to train the Carvana and Camvid models. It also contains a number of python scripts which contain classes and functions
   which are used by the python notebooks.
2. The src/tests folder contains python notebooks which have code used for testing the models trained above.
3. The src/experiments folder contain a number of python notebooks which were used for hyperparameter tuning and for visualizing the dataset, etc.

## Code Summary
1. segnet_model.py
   This file contains the SegnetModel class which provides functionality to build, load and save the SegNet model.

2. base_data_manager.py
   This file contains the DataManagerBase base class which needs to be implemented to provide functionality to loads and manages the datasets.

3. kaggle_data_manager.py
   This file contains the KaggleDataManager class which implements the DataManagerBase class and has functions to load images from the Carvana dataset.
   It also has functionality to split them into training and validation sets, and functions to return data in batches.

4. kaggle_data_generator.py
   This file contains the KaggleDataGenerator class which provides functionality to generate augmented data for our datasets.

5. camvid_data_manager.py
   This file contains the CamvidDataManager class which implements DataManagerBase class and has functions to load images(train, validation and test) for the Camvid dataset, 
   and functions to return data in batches.

6. utils.py
   It has utility functions for logging, loading, plotting images, reading data from the config files, etc.

7. training_utils.py
   It has utility functions to create and train model and also load a saved model and test it

8. camvid_test_util.py
   It has utility functions to load saved models and test it

9. experiments folder
   It contains .ipynb python notebooks which have code used for experiments with the datasets, hyperparameter tuning and tests for these models.

10. tests folder
	It contains .ipynb python notebooks which have code used for testing our models.


## Usage instructions

1. Install Python 3.6.5(Anaconda)
2. pip install the packages mentioned in the import.txt
3. Build and open the code in Jupyter Notebook by running the following command in Anaconda Prompt
		jupyter notebook --notebook-dir= <src directory>
		ex: src_directory = "/home/ubuntu/foo/segnet/src"
4. Download the data from below locations for training and testing the various models.
		Carvana Dataset: https://drive.google.com/open?id=1iF4JVnFFebWasXVgg8Otw39sQuVoJ5BB
		Please unzip the file downloaded from here and the zip files in the extracted location before attempting to train or test the models.
		CamVid Dataset: https://drive.google.com/open?id=1k_jf0tqmgyjR9W1G2L1aV6GafyttPmbv
		Please download the data from the above location.
5. Configure the image_segmentation.ini file
6. Run the python notebooks in the src folder for training the models and the ones in the tests folder for testing the models.



import cv2
import kaggle_data_manager as kdm
import kaggle_data_generator as kdg
import keras
import numpy as np
import os
import random
import segnet_model
import tensorflow as tf
import utils
from keras.callbacks import TensorBoard, EarlyStopping, ModelCheckpoint
from scipy import misc
from time import time
from utils import init_logging, log_debug, log_info, log_warning, log_critical, log_exception

def build_or_load_model(config_file_name, optimizer = "optimizers.Adam()", loss_function = segnet_model.LossType.DICE_LOSS, num_classes = 1):
    segnet_instance = segnet_model.SegnetModel(config_file_name)
    model = segnet_instance.load_model("optimizers.Adam(lr=0.0001)", loss_type = loss_function)
    if (model == None):
        log_info("Failed to find saved model data. Building model for training.")
        model = segnet_instance.build("optimizers.Adam(lr=0.0001)", loss_type = loss_function)
    return segnet_instance, model

def train_model_helper(config_file_name, section_name, segnet_instance, model, data_manager = None):
    if (data_manager == None):
        data_manager = kdm.KaggleDataManager(config_file_name)

    ## 3. Model training.
    logs_path = utils.get_config_value(config_file_name, section_name, "logFilePath")
    log_info("Log file path is %s" %logs_path)

    checkpoints_path = utils.get_config_value(config_file_name, section_name, "modelCheckpointsPath")
    log_info("Model checkpoints path is %s" %checkpoints_path)

    batch_size = int(utils.get_config_value(config_file_name, section_name, "BatchSize"))
    log_info("Batch size is %d" %batch_size)

    epochs = int(utils.get_config_value(config_file_name, section_name, "Epochs"))
    log_info("Epochs is %d" %epochs)

    # https://fizzylogic.nl/2017/05/08/monitor-progress-of-your-keras-based-neural-network-using-tensorboard/
    tensorboard = TensorBoard(log_dir=logs_path + "/{}".format(time()))

    # https://chrisalbon.com/deep_learning/keras/neural_network_early_stopping/
    callbacks = [EarlyStopping(monitor='val_loss', verbose = 1, patience=5),
                 ModelCheckpoint(filepath=checkpoints_path, monitor='val_loss', verbose = 1, save_best_only=True,
                                save_weights_only = True),
                tensorboard]

    steps_per_epoch = data_manager.get_number_of_training_records() // batch_size
    validation_steps = data_manager.get_number_of_validation_records() // batch_size

    data_manager.init_generator(kdg.BatchType.TRAIN_DATA)
    data_manager.init_generator(kdg.BatchType.VALIDATION_DATA)

    log_info("Steps per epoch is %d" %steps_per_epoch)
    log_info("Validation steps is %d" %validation_steps)

    history = model.fit_generator(
            data_manager.get_next_batch_train(batch_size), 
            steps_per_epoch = steps_per_epoch, 
            epochs = epochs,
            callbacks = callbacks,
            verbose = 1,
            validation_data = data_manager.get_next_batch_validate(batch_size),
            validation_steps = validation_steps)

    utils.plot_training_graphs(history, "Training and Validation Loss", "loss", "val_loss", "train_loss", "validation_loss")
    utils.plot_training_graphs(history, "Training and Validation Accuracy (Dice coefficients)", "dice_coef", "val_dice_coef", "train_accuracy", "validation_accuracy")

    log_info("Done training model")
    log_info("\nSaving model")
    segnet_instance.save()

def test_carvana_model(folder, count, config_file_name, loss_function):
    segnet_instance, model = build_or_load_model(config_file_name, "optimizer.Adam(lr = 0.0001)", loss_function)
    log_info("Loaded model successfully")

    data_path = utils.get_config_value(config_file_name, "General", "dataPath")
    image_width = int(utils.get_config_value(config_file_name, "General", "imageWidth"))
    image_height = int(utils.get_config_value(config_file_name, "General", "imageHeight"))
    seed = int(utils.get_config_value(config_file_name, "General", "imageAugmentationSeed"))
    
    log_info("Image width %d, Image height %d" %(image_width, image_height))
    log_info("datapath %s" %(data_path))

    if (folder == "/val"):
        data_manager = kdm.KaggleDataManager(config_file_name)

        validate_images = data_manager.get_validation_images()
        validate_masks = data_manager.get_validation_masks()

        random_indices = random.sample(range(len(validate_images)), count)

        for index in random_indices:
            images = []
            cmaps = []

            image_normalized = np.array(validate_images[index], np.float32) / 255
            predicted_mask = model.predict(np.expand_dims(image_normalized, axis = 0))
            predicted_mask = np.squeeze(predicted_mask)
            predicted_mask[predicted_mask > 0.5] = 1
            predicted_mask[predicted_mask <= 0.5] = 0

            images.append(validate_images[index])
            images.append(validate_masks[index])
            images.append(predicted_mask)

            cmaps.append(None)
            cmaps.append('gray')
            cmaps.append('gray')

            utils.plot_images(images, cmaps)
    else:
        random.seed(seed)
        filenames = random.sample(os.listdir(data_path + folder), count)
        for image_path in filenames:
            images = []
            cmaps = []

            image = misc.imread(data_path + folder + "/" + image_path)
            log_info("Image path is %s" %image_path)
            image = cv2.resize(image, (image_width, image_height))
            image_normalized = np.array(image, np.float32) / 255
            predicted_mask = model.predict(np.expand_dims(image_normalized, axis = 0))
            predicted_mask = np.squeeze(predicted_mask)
            predicted_mask[predicted_mask > 0.5] = 1
            predicted_mask[predicted_mask <= 0.5] = 0
            images.append(image)
            cmaps.append(None)
        
            if folder  != "/test":
                image_path = image_path.replace(".jpg", "_mask.gif")
                original_mask_folder = folder + "_masks"
                original_mask = misc.imread(data_path + "/" + original_mask_folder + "/" + image_path)
                original_mask = cv2.resize(original_mask, (image_width, image_height))
                images.append(original_mask)
                cmaps.append('gray')

            images.append(predicted_mask)
            cmaps.append('gray')
            utils.plot_images(images, cmaps)
